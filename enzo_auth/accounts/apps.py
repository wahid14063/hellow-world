from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'accounts'

class TasksConfig(AppConfig):
    name = 'tasks'
