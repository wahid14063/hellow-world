from django.test import TestCase, Client
from django.urls import reverse
from tasks.models import Task
import json

class TestViews(TestCase):

    def setUp(self):
        #setup code
        self.client = Client()
        self.list_url = reverse('list')
        self.report_generation_url = reverse('report_generation')
        #self.update_task_url = reverse('update_task', args = ['1'])

    def test_project_list_GET(self):
        #test code
        response = self.client.get(self.list_url)

        #assertions
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/list.html')

    """def test_project_update_task_GET(self):
        #test code
        response = self.client.get(self.update_task_url)

        #assertions
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/update_task.html')"""

    def test_report_generation_GET(self):
        #test code
        response = self.client.get(self.report_generation_url)

        #assertions
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/report_generation.html')

