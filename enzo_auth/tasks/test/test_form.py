from django.test import SimpleTestCase
from tasks.forms import TaskForm

class TestForms(SimpleTestCase):
    # 3 simple test case for Testing form 
    def test_form_is_valid(self):
        
        form=TaskForm(data={
            'title':'Do assginment'
        })
        self.assertTrue(form.is_valid())

    def test_form_no_data(self):
        form=TaskForm(data={})
        self.assertFalse(form.is_valid())
    
    def test_form_no_data_len(self):
        form=TaskForm(data={})
        self.assertEquals(len(form.errors),1)