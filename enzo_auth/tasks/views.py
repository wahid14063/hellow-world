import csv

from django.shortcuts import render, redirect
from django.http import HttpResponse

from .models import *
from .forms import *

# Create your views here.
#Made tasks universal in order to satisfy report generation.

taskList = []

#General: The index function is the foundation of the task management system, allowing for the creation of tasks by the user.
#Technical: The index function uses the Task class created in models.py and allows for tasks to be created via the html post function.
#Task objects are only created in the index function, they are not further modified.
#The index function returns the list.html file located at /enzo_auth/tasks/templates/tasks/list.html
def index(request):

        tasks = Task.objects.all()
        form = TaskForm()

        if request.method =='POST':
                form = TaskForm(request.POST)
                if form.is_valid():
                        form.save()
                return redirect('/')


        context = {'tasks':tasks, 'form':form}
        return render(request, 'tasks/list.html', context)

#The update task function takes a task which already exists and allows the user to modify it so that the task is marked as complete.
def updateTask(request, pk):
	task = Task.objects.get(id=pk)

	form = TaskForm(instance=task)

	if request.method == 'POST':
		form = TaskForm(request.POST, instance=task)
		if form.is_valid():
			form.save()
			return redirect('/')

	context = {'form':form}

	return render(request, 'tasks/update_task.html', context)

#The delete task function takes a task which already exists and allows the user to delete the task completely from the task list.
def deleteTask(request, pk):
	item = Task.objects.get(id=pk)

	if request.method == 'POST':
		item.delete()
		return redirect('/')

	context = {'item':item}
	return render(request, 'tasks/delete.html', context)

#The report generation function allows the user to export all current task information to a csv file located in the enz_auth directory.
def reportGeneration(request):
        records = Task.objects.all()

        if request.method == 'POST':
                with open('task_report.csv', 'w', newline='') as file:
                        writer = csv.writer(file)
                        writer.writerow(["name","completed","created"])
                        for each in records:
                                tempList = []
                                tempList.append(each.title)
                                tempList.append(each.complete)
                                tempList.append(each.created)
                                writer.writerow(tempList)
                        return redirect('/')
        
        context = {'records':records}
        return render(request, 'tasks/report_generation.html', context)


